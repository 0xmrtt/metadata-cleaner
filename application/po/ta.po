# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# K.B.Dharun Krishna <kbdharunkrishna@gmail.com>, 2022, 2023.
# "K.B.Dharun Krishna" <kbdharunkrishna@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-08 19:53+0000\n"
"PO-Revision-Date: 2023-04-11 06:48+0000\n"
"Last-Translator: \"K.B.Dharun Krishna\" <kbdharunkrishna@gmail.com>\n"
"Language-Team: Tamil <https://hosted.weblate.org/projects/metadata-cleaner/"
"application/ta/>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.17-dev\n"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: application/data/ui/Window.ui:107 application/metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr "மெட்டாடேட்டா கிளீனர்"

#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr "உங்கள் கோப்புகளிலிருந்து மெட்டாடேட்டாவை சுத்தம் செய்யவும்"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: application/data/fr.romainvigier.MetadataCleaner.desktop.in:15
msgid "Metadata;Remover;Cleaner;"
msgstr "மெட்டாடேட்டா;நீக்குபவர்;துப்புரவாளர்;"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr "கோப்புகளில் உள்ள மெட்டாடேட்டாவைப் பார்த்து சுத்தம் செய்யவும்"

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:26
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"ஒரு கோப்பில் உள்ள மெட்டாடேட்டா உங்களைப் பற்றி நிறைய சொல்ல முடியும். ஒரு படம் எப்போது, "
"எங்கு எடுக்கப்பட்டது, எந்த கேமரா பயன்படுத்தப்பட்டது என்பது பற்றிய தரவை கேமராக்கள் பதிவு "
"செய்கின்றன. அலுவலக பயன்பாடுகள் தானாக ஆசிரியர் மற்றும் நிறுவனத்தின் தகவல்களை ஆவணங்கள் "
"மற்றும் விரிதாள்களில் சேர்க்கும். இது முக்கியமான தகவல் மற்றும் நீங்கள் அதை வெளியிட "
"விரும்பாமல் இருக்கலாம்."

#: application/data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""
"இந்தக் கருவி உங்கள் கோப்புகளில் உள்ள மெட்டாடேட்டாவைப் பார்க்கவும்,முடிந்தவரை அதிலிருந்து "
"விடுபடவும் உங்களை அனுமதிக்கிறது."

#: application/data/gtk/help-overlay.ui:15
msgid "Files"
msgstr "கோப்புகள்"

#: application/data/gtk/help-overlay.ui:18
#: application/data/ui/AddFilesButton.ui:49
msgid "Add files"
msgstr "கோப்புகளைச் சேர்க்கவும்"

#: application/data/gtk/help-overlay.ui:24
#: application/data/ui/AddFilesButton.ui:56
msgid "Add folders"
msgstr "கோப்புறைகளைச் சேர்க்கவும்"

#: application/data/gtk/help-overlay.ui:30
msgid "Clean metadata"
msgstr "மெட்டாடேட்டாவை சுத்தம் செய்யுங்கள்"

#: application/data/gtk/help-overlay.ui:36
msgid "Clear all files from window"
msgstr "சாளரத்திலிருந்து எல்லா கோப்புகளையும் அழிக்கவும்"

#: application/data/gtk/help-overlay.ui:44
msgid "General"
msgstr "பொது"

#: application/data/gtk/help-overlay.ui:47
msgid "New window"
msgstr "புதிய சாளரம்"

#: application/data/gtk/help-overlay.ui:53
msgid "Close window"
msgstr "சாளரத்தை மூடு"

#: application/data/gtk/help-overlay.ui:59
msgid "Quit"
msgstr "வெளியேறு"

#: application/data/gtk/help-overlay.ui:65
msgid "Keyboard shortcuts"
msgstr "விசைப்பலகை குறுக்குவழிகள்"

#: application/data/gtk/help-overlay.ui:71
msgid "Help"
msgstr "உதவி"

#: application/data/ui/AddFilesButton.ui:21
msgid "_Add Files"
msgstr "_கோப்புகளைச் சேர்க்கவும்"

#: application/data/ui/AddFilesButton.ui:33
msgid "Add _Folders"
msgstr "கோப்புறைகளைச்_சேர்க்கவும்"

#: application/data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr "_சுத்தம்"

#: application/data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr "உங்கள் கோப்புகளை காப்புப் பிரதி எடுத்திருப்பதை உறுதிசெய்யவும்!"

#: application/data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr "கோப்புகளை சுத்தம் செய்தவுடன், திரும்பப் போவதில்லை."

#: application/data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr "மீண்டும் என்னிடம் சொல்லாதே"

#: application/data/ui/CleaningWarningDialog.ui:25
#: application/data/ui/StatusIndicator.ui:37
msgid "Cancel"
msgstr "ரத்துசெய்"

#: application/data/ui/CleaningWarningDialog.ui:30
msgid "Clean"
msgstr "சுத்தம் செய்"

#: application/data/ui/EmptyView.ui:13
msgid "Clean Your Traces"
msgstr "உங்கள் தடயங்களை சுத்தம் செய்யவும்"

#: application/data/ui/EmptyView.ui:39
msgid "Learn more about metadata and the cleaning process limitations"
msgstr "மெட்டாடேட்டா மற்றும் சுத்தம் செய்யும் செயல்முறை வரம்புகள் பற்றி மேலும் அறிக"

#: application/data/ui/FileRow.ui:11
msgid "Remove file from list"
msgstr "பட்டியலிலிருந்து கோப்பை அகற்று"

#: application/data/ui/FileRow.ui:128
msgid "Warning"
msgstr "எச்சரிக்கை"

#: application/data/ui/FileRow.ui:146
msgid "Error"
msgstr "பிழை"

#: application/data/ui/FileRow.ui:186
msgid "Cleaned"
msgstr "சுத்தம் செய்யப்பட்டது"

#: application/data/ui/MenuButton.ui:10
msgid "_New Window"
msgstr "_புதிய சாளரம்"

#: application/data/ui/MenuButton.ui:14
msgid "_Clear Window"
msgstr "_சாளரத்தை அழிக்கவும்"

#: application/data/ui/MenuButton.ui:20
msgid "_Help"
msgstr "_உதவி"

#: application/data/ui/MenuButton.ui:25
msgid "_Keyboard Shortcuts"
msgstr "_விசைப்பலகை குறுக்குவழிகள்"

#: application/data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr "_மெட்டாடேட்டா கிளீனர் பற்றி"

#: application/data/ui/MenuButton.ui:38
msgid "Main menu"
msgstr "முதன்மை பட்டியல்"

#: application/data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr "சுத்தம் செய்யும் அமைப்புகள்"

#: application/data/ui/SettingsButton.ui:27
msgid "Lightweight Cleaning"
msgstr "இலகுரக சுத்தம்"

#: application/data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr "இலகுரக சுத்தம் செய்வது பற்றி மேலும் அறிக"

#: application/data/ui/Window.ui:80
msgid "Details"
msgstr "விவரங்கள்"

#: application/data/ui/Window.ui:86
msgid "Close"
msgstr "மூடுங்கள்"

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`) or your website (Example: `Name https://example.org/`). If names are already present, do not remove them and add yours on a new line.
#: application/data/ui/Window.ui:125
msgid "translator-credits"
msgstr "K.B.Dharun Krishna <kbdharunkrishna@gmail.com>"

#: application/data/ui/Window.ui:128
msgid "Choose files to clean"
msgstr "சுத்தம் செய்ய கோப்புகளைத் தேர்ந்தெடுக்கவும்"

#: application/data/ui/Window.ui:138
msgid "Choose folders to clean"
msgstr "சுத்தம் செய்ய கோப்புறைகளைத் தேர்ந்தெடுக்கவும்"

#: application/metadatacleaner/modules/file.py:244
msgid "An error occured during the cleaning."
msgstr "சுத்தம் செய்யும் போது ஒரு பிழை ஏற்பட்டது."

#: application/metadatacleaner/modules/file.py:247
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr "சுத்தம் செய்யும் போது ஏதோ தவறு நடந்தது, சுத்தம் செய்யப்பட்ட கோப்பு கிடைக்கவில்லை"

#: application/metadatacleaner/ui/detailsview.py:58
msgid "The File Has Been Cleaned"
msgstr "கோப்பு சுத்தம் செய்யப்பட்டுள்ளது"

#: application/metadatacleaner/ui/detailsview.py:60
msgid ""
"Known metadata have been removed, however the cleaning process has some "
"limitations."
msgstr ""
"அறியப்பட்ட மெட்டாடேட்டா அகற்றப்பட்டது, இருப்பினும் சுத்தம் செய்யும் செயல்முறைக்கு சில "
"வரம்புகள் உள்ளன."

#: application/metadatacleaner/ui/detailsview.py:63
msgid "Learn more"
msgstr "மேலும் அறிக"

#: application/metadatacleaner/ui/detailsview.py:76
msgid "Unable to Read the File"
msgstr "கோப்பைப் படிக்க முடியவில்லை"

#: application/metadatacleaner/ui/detailsview.py:77
msgid "File Type not Supported"
msgstr "கோப்பு வகை ஆதரிக்கப்படவில்லை"

#: application/metadatacleaner/ui/detailsview.py:79
msgid "Unable to Check for Metadata"
msgstr "மெட்டாடேட்டாவைச் சரிபார்க்க முடியவில்லை"

#: application/metadatacleaner/ui/detailsview.py:80
msgid "No Known Metadata"
msgstr "அறியப்பட்ட மெட்டாடேட்டா இல்லை"

#: application/metadatacleaner/ui/detailsview.py:82
msgid "Unable to Remove Metadata"
msgstr "மெட்டாடேட்டாவை அகற்ற முடியவில்லை"

#: application/metadatacleaner/ui/detailsview.py:86
msgid "The file will be cleaned anyway to be sure."
msgstr "உறுதியாக இருக்க கோப்பு எப்படியும் சுத்தம் செய்யப்படும்."

#: application/metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr "அனைத்து ஆதரிக்கப்படும் கோப்புகள்"

#: application/metadatacleaner/ui/folderchooserdialog.py:22
msgid "Add files from subfolders"
msgstr "துணை கோப்புறைகளிலிருந்து கோப்புகளைச் சேர்க்கவும்"

#: application/metadatacleaner/ui/statusindicator.py:37
msgid "Processing file {}/{}"
msgstr "{}/{} கோப்பைச் செயலாக்குகிறது"

#: application/metadatacleaner/ui/statusindicator.py:39
msgid "Cleaning file {}/{}"
msgstr "கோப்பை சுத்தம் செய்கிறது {}/{}"

#: application/metadatacleaner/ui/window.py:112
msgid "Libraries"
msgstr "நூலகங்கள்"

#~ msgid "Clean without warning"
#~ msgstr "எச்சரிக்கை இல்லாமல் சுத்தம் செய்யுங்கள்"

#~ msgid "Clean the files without showing the warning dialog"
#~ msgstr "எச்சரிக்கை உரையாடலைக் காட்டாமல் கோப்புகளை சுத்தம் செய்யவும்"

#~ msgid "Lightweight cleaning"
#~ msgstr "இலகுரக சுத்தப்படுத்துதல்"

#~ msgid "Don't make destructive changes to files but may leave some metadata"
#~ msgstr ""
#~ "கோப்புகளில் அழிவுகரமான மாற்றங்களைச் செய்ய வேண்டாம், ஆனால் சில மெட்டாடேட்டாவை "
#~ "விட்டுவிடலாம்"

#~ msgid "Window width"
#~ msgstr "சாளர அகலம்"

#~ msgid "Saved width of the window"
#~ msgstr "சாளரத்தின் சேமிக்கப்பட்ட அகலம்"

#~ msgid "Window height"
#~ msgstr "சாளரத்தின் உயரம்"

#~ msgid "Saved height of the window"
#~ msgstr "சாளரத்தின் சேமிக்கப்பட்ட உயரம்"

#~ msgid "Updated translations"
#~ msgstr "புதுப்பிக்கப்பட்ட மொழிபெயர்ப்புகள்"

#~ msgid "Improved user interface"
#~ msgstr "மேம்படுத்தப்பட்ட பயனர் இடைமுகம்"

#~ msgid "New translations"
#~ msgstr "புதிய மொழிபெயர்ப்புகள்"

#~ msgid "Bug fixes"
#~ msgstr "பிழை திருத்தங்கள்"

#~ msgid "New button to add folders"
#~ msgstr "கோப்புறைகளைச் சேர்க்க புதிய பொத்தான்"

#~ msgid "Improved adaptive user interface"
#~ msgstr "மேம்படுத்தப்பட்ட தகவமைப்பு பயனர் இடைமுகம்"

#~ msgid "New help pages"
#~ msgstr "புதிய உதவி பக்கங்கள்"

#~ msgid "One-click cleaning, no need to save after cleaning"
#~ msgstr "ஒரே கிளிக்கில் சுத்தம் செய்தல், சுத்தம் செய்த பிறகு சேமிக்க வேண்டிய அவசியமில்லை"

#~ msgid "Persistent lightweight cleaning option"
#~ msgstr "தொடர்ந்து இலகுரக சுத்தம் செய்யும் விருப்பம்"

#~ msgid "Files with uppercase extension can now be added"
#~ msgstr "பெரிய எழுத்து நீட்டிப்பு கொண்ட கோப்புகளை இப்போது சேர்க்கலாம்"

#~ msgid "About"
#~ msgstr "பற்றி"

#~ msgid "Chat on Matrix"
#~ msgstr "மேட்ரிக்ஸில் பேசுங்கள்"

#~ msgid "View the code on GitLab"
#~ msgstr "GitLab இல் குறியீட்டைக் காண்க"

#~ msgid "Translate on Weblate"
#~ msgstr "வெப்லேட்டில் மொழிபெயர்க்கவும்"

#~ msgid "Support us on Liberapay"
#~ msgstr "Liberapay இல் எங்களை ஆதரிக்கவும்"

#~ msgid "Credits"
#~ msgstr "வரவுகள்"

#~ msgid "Code"
#~ msgstr "குறியீடு"

#~ msgid "Artwork"
#~ msgstr "கலைப்படைப்பு"

#~ msgid "Documentation"
#~ msgstr "ஆவணப்படுத்தல்"

#~ msgid "Translation"
#~ msgstr "மொழிபெயர்ப்பு"

#~ msgid ""
#~ "This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> to "
#~ "parse and clean the metadata. Show them some love!"
#~ msgstr ""
#~ "இந்த நிரல் மெட்டாடேட்டாவை அலசவும் சுத்தம் செய்யவும் <a href=\"https://0xacab.org/"
#~ "jvoisin/mat2\">mat2</a> ஐப் பயன்படுத்துகிறது. அவர்களுக்கு கொஞ்சம் அன்பைக் காட்டுங்கள்!"

#~ msgid ""
#~ "The source code of this program is released under the terms of the <a "
#~ "href=\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 or later</"
#~ "a>. The original artwork and translations are released under the terms of "
#~ "the <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA "
#~ "4.0</a>."
#~ msgstr ""
#~ "இந்த நிரலின் மூலக் குறியீடு <a href=\"https://www.gnu.org/licenses/gpl-3.0."
#~ "html\">GNU GPL 3.0 அல்லது அதற்குப் பிறகு</a> விதிமுறைகளின் கீழ் வெளியிடப்பட்டது. "
#~ "அசல் கலைப்படைப்பு மற்றும் மொழிபெயர்ப்புகள் <a href=\"https://creativecommons.org/"
#~ "licenses/by-sa/4.0/\">CC BY-SA 4.0</a> விதிமுறைகளின் கீழ் வெளியிடப்பட்டது."

#~ msgid "Adding files…"
#~ msgstr "கோப்புகளைச் சேர்க்கிறது…"
