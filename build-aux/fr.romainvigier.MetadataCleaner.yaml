# SPDX-FileCopyrightText: Metadata Cleaner contributors
# SPDX-License-Identifier: GPL-3.0-or-later

app-id: fr.romainvigier.MetadataCleaner
default-branch: develop
runtime: org.gnome.Platform
runtime-version: '44'
sdk: org.gnome.Sdk
command: metadata-cleaner
separate-locales: false

add-extensions:
  org.freedesktop.Platform.ffmpeg-full:
    directory: lib/ffmpeg
    add-ld-path: '.'
    version: '22.08'

finish-args:
  - --socket=wayland
  - --socket=fallback-x11
  - --share=ipc
  - --device=dri

cleanup-commands:
  - mkdir -p $FLATPAK_DEST/lib/ffmpeg

cleanup:
  - /include
  - /lib/pkgconfig
  - /share/doc
  - /share/man
  - '*.a'
  - '*.la'

modules:

  - name: metadata-cleaner
    buildsystem: meson
    config-opts:
      - -Ddevel=true
    sources:
      - type: dir
        path: ../
    modules:
      - name: media-types
        buildsystem: simple
        build-commands:
          - mkdir -p $FLATPAK_DEST/share
          - install -D mime.types $FLATPAK_DEST/share/
        sources:
          - type: git
            url: https://salsa.debian.org/debian/media-types.git
            tag: 10.0.0
            commit: 4ef946d251287545a1498345bc08b587da85a316

      - name: python3-libmat2
        buildsystem: simple
        build-commands:
          - python3 setup.py build
          - python3 setup.py install --optimize=1 --single-version-externally-managed --prefix=$FLATPAK_DEST --root=/
        ensure-writable:
          - /lib/python3*/site-packages/easy-install.pth
        sources:
          - type: git
            url: https://0xacab.org/jvoisin/mat2.git
            tag: 0.13.2
            commit: 66a36f6b15b0a9fdc792aabe3d0956b3fc680464
        modules:
          - name: exiftool
            buildsystem: simple
            build-commands:
              - perl Makefile.PL
              - make install
            cleanup:
              - '*.pod'
            sources:
              - type: git
                url: https://github.com/exiftool/exiftool.git
                tag: '12.58'
                commit: 4e32ffefc5bd393a692a1ad375c966a883b20291
            modules:
              - name: perl
                buildsystem: simple
                build-commands:
                  - ./Configure -des -Dprefix=$FLATPAK_DEST -Dman1dir=none -Dman3dir=none
                  - make
                  - make install
                post-install:
                  # Fix wrong permissions
                  - chmod -R u+w $FLATPAK_DEST/lib/perl5
                cleanup:
                  - /bin/corelist
                  - /bin/cpan
                  - /bin/enc2xs
                  - /bin/encguess
                  - /bin/h2ph
                  - /bin/h2xs
                  - /bin/instmodsh
                  - /bin/json_pp
                  - /bin/libnetcfg
                  - /bin/perl5*
                  - /bin/perlbug
                  - /bin/perldoc
                  - /bin/perlivp
                  - /bin/perlthanks
                  - /bin/piconv
                  - /bin/pl2pm
                  - /bin/pod2html
                  - /bin/pod2man
                  - /bin/pod2text
                  - /bin/pod2usage
                  - /bin/podchecker
                  - /bin/prove
                  - /bin/ptar
                  - /bin/ptardiff
                  - /bin/ptargrep
                  - /bin/shasum
                  - /bin/splain
                  - /bin/streamzip
                  - /bin/xsubpp
                  - /bin/zipdetails
                  - '*.pod'
                sources:
                  - type: git
                    url: https://github.com/Perl/perl5.git
                    tag: v5.36.0
                    commit: b3c502b607191da0e743a4fa34501a05442305b3

          - name: poppler
            buildsystem: cmake
            config-opts:
              - -DENABLE_BOOST=OFF
              - -DENABLE_SPLASH=OFF
              - -DBUILD_GTK_TESTS=OFF
              - -DBUILD_QT5_TESTS=OFF
              - -DBUILD_QT6_TESTS=OFF
              - -DBUILD_CPP_TESTS=OFF
              - -DBUILD_MANUAL_TESTS=OFF
              - -DENABLE_CPP=OFF
              - -DENABLE_UTILS=OFF
              - -DENABLE_QT5=OFF
              - -DENABLE_QT6=OFF
            sources:
              - type: git
                url: https://anongit.freedesktop.org/git/poppler/poppler.git
                tag: poppler-23.03.0
                commit: 051c2601ecc35864cad6172db8387b64951cf859
            modules:
              - name: poppler-data
                buildsystem: cmake
                sources:
                  - type: git
                    url: https://anongit.freedesktop.org/git/poppler/poppler-data.git
                    tag: POPPLER_DATA_0_4_11
                    commit: 0ea8e08c257f58cc967f7da59e2d0eac20f1b455

          - name: python3-mutagen
            buildsystem: simple
            build-commands:
              - python3 setup.py build
              - python3 setup.py install --optimize=1 --single-version-externally-managed --prefix=$FLATPAK_DEST --root=/
            ensure-writable:
              - /lib/python3*/site-packages/easy-install.pth
            cleanup:
              - /bin/mid3cp
              - /bin/mid3iconv
              - /bin/mid3v2
              - /bin/moggsplit
              - /bin/mutagen-inspect
              - /bin/mutagen-pony
            sources:
              - type: git
                url: https://github.com/quodlibet/mutagen.git
                tag: release-1.46.0
                commit: 5b967acd2392b44a5c21d6a4c34bf624c9aa3fe9
